// console.log('Hello, World');

let trainer = {
	name: 'Ash Ketchup',
	age: 10,
	pokemon: ['Pikachu', 'Charizard', 'Squirtle', 'Bulbasaur'],
	friends: {
		hoenn: ['May', 'Max'],
		kanto: ['Jessie', 'James']
	},
	talk(index) {
		console.log(`${this.pokemon[index]}! I choose you!`);
	}
}
console.log(trainer);

console.log('Result of dot notation:')
console.log(trainer.name);

console.log('Result of square bracket notation:');
console.log(trainer['pokemon']);

console.log('Result of talk method:');
trainer.talk(2);


function Pokemon(name, level) {
	this.Name = name;
	this.Lvl = level;
	this.Hp = level * 2;
	this.Atk = level;
	this.tackle = function(target) {
		console.log(`${this.Name} tackled ${target.Name}.`);
		target.Hp -= this.Atk;
		console.log(`${target.Name}'s health is now reduced to ${target.Hp}.`)
		if(target.Hp <= 0) {
			target.faint();
		}
	}
	this.faint = function() {
		console.log(`${this.Name} fainted!`);
	}
}

let pikachu = new Pokemon(trainer.pokemon[0], 12);
let geodude = new Pokemon('Geodude', 8);
let mewtwo = new Pokemon('Mewtwo', 100);
console.log(pikachu);
console.log(geodude);
console.log(mewtwo);

mewtwo.tackle(geodude);

